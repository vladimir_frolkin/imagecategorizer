/*
 * Copyright (c) 2015, jet800
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.jet800.imagecategorizer;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import net.jet800.imagecategorizer.category.FileCategory;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author jet800
 */
public class ImageDirectoryCategorizerTest {

    private final DateTimeFormatter formatter;

    public ImageDirectoryCategorizerTest() {
        formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
    }

    /**
     * Test of categorize method, of class ImageDirectoryCategorizer.
     */
    @org.junit.Test
    public void testCategorize_Iterable() {
        System.out.println("categorize");

        List<Image> images = Arrays.asList(
                newImage("m1", "2015-08-01"),
                newImage("m2", "2015-08-02"),
                newImage("m3", "2015-08-02"),
                newImage("m2", "2015-08-01"),
                newImage("m3", "2015-08-10")
        );

        List<FileCategory> result = ImageDirectoryCategorizer.categorize(images);
        assertEquals(result.size(), 1);
    }

    Image newImage(String model, String date) {
        return new Image(new File("/tmp/" + model + "-" + date), formatter.parseDateTime(date), model);
    }
}
