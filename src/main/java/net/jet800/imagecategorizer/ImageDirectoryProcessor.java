/*
 * Copyright (c) 2015, jet800
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.jet800.imagecategorizer;

import java.io.File;
import java.io.FileFilter;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.jet800.imagecategorizer.category.FileCategory;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;

/**
 *
 * @author jet800
 */
public class ImageDirectoryProcessor {

    private static final ThreadFactory TF = new BasicThreadFactory.Builder()
            .namingPattern("imagecategorizer-%d")
            .daemon(true)
            .priority(Thread.MIN_PRIORITY)
            .build();

    private final int timeout;
    private final int numThreads;
    private final File rootDirectory;

    /**
     * Creates a new instance of <code>ImageDirectoryProcessor</code>.
     *
     * @param timeout timeout for waiting for results of processing a
     * sub-directory.
     * @param numThreads size of internal pool to run categorization. Naturally
     * how many directories should be processed simulateneously.
     * @param rootDirectory directory to start scanning from.
     * @throws NullPointerException if any parameter is null.
     * @throws IllegalArgumentException if either timeout or numThreads is not
     * positive or rootDirectory is not directory.
     */
    public ImageDirectoryProcessor(int timeout, int numThreads, File rootDirectory) {
        Validate.notNull(timeout, "Timeout value could not be empty!");
        Validate.notNull(numThreads, "Number of threads could not be empty!");
        Validate.notNull(rootDirectory, "You must specify root directory to process!");
        Validate.isTrue(timeout > 0, "Timeout value should be positive: %d", timeout);
        Validate.isTrue(numThreads > 0, "Number of threads should be positive: %d", numThreads);
        Validate.isTrue(rootDirectory.isDirectory(), "Specified path should be directory: %s", rootDirectory);
        this.timeout = timeout;
        this.numThreads = numThreads;
        this.rootDirectory = rootDirectory;
    }

    /**
     * Executes categorization process. For each directory under root a
     * <code>List</code> will be created, which would contain
     * <code>FileCategory</code> for each distinct group of images with either
     * same model or filming date.
     *
     * @return <code>List</code> of <code>List</code>s of
     * <code>FileCategory</code> with categorized images.
     * @throws InterruptedException if the current thread was interrupted while
     * waiting.
     */
    public List<List<FileCategory>> process() throws InterruptedException {
        ExecutorService es = Executors.newFixedThreadPool(numThreads, TF);
        List<Future<List<FileCategory>>> futures = new LinkedList<>();
        for (File f : rootDirectory.listFiles((FileFilter) DirectoryFileFilter.DIRECTORY)) {
            futures.add(es.submit(new ImageDirectoryCategorizer(f)));
        }
        List<List<FileCategory>> result = new LinkedList<>();
        for (Future<List<FileCategory>> future : futures) {
            try {
                result.add(future.get(timeout, TimeUnit.SECONDS));
            } catch (ExecutionException ex) {
                Logger.getLogger(ImageDirectoryProcessor.class.getName()).log(Level.SEVERE, "Abnormal process termination", ex);
            } catch (TimeoutException ex) {
                Logger.getLogger(ImageDirectoryProcessor.class.getName()).log(Level.SEVERE, "Processing a directory took longer than specified timeout value[%d]", timeout);
            }
        }
        return result;
    }
}
