/*
 * Copyright (c) 2015, jet800
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.jet800.imagecategorizer;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Descriptor;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import org.joda.time.DateTime;

/**
 *
 * @author jet800
 */
public class Image {

    private final File file;
    private final Metadata metadata;
    private DateTime dateTime;
    private String model;

    /**
     * Constructs an image from given file, reading it's metadata.
     *
     * @param file a file from which the image data may be read.
     * @throws ImageProcessingException for general processing errors.
     * @throws IOException if an I/O error occurs.
     */
    public Image(File file) throws ImageProcessingException, IOException {
        this.file = file;
        this.metadata = ImageMetadataReader.readMetadata(file);
        this.dateTime = null;
        this.model = null;
        extractDate();
        extractModel();
    }

    /**
     * Constructs an image from given file, <b>without</b> reading it's metadata.
     *
     * @param file a file, where image resides.
     * @param dateTime filming date of given image.
     * @param model camera model used for filming.
     */
    public Image(File file, DateTime dateTime, String model) {
        this.file = file;
        this.metadata = null;
        this.dateTime = dateTime;
        this.model = model;
    }

    /**
     * Constructs an image from given file, <b>without</b> reading it's metadata.
     *
     * @param file a file, where image resides.
     * @param date filming date of given image.
     * @param model camera model used for filming.
     */
    public Image(File file, Date date, String model) {
        this.file = file;
        this.metadata = null;
        this.dateTime = new DateTime(date).withTimeAtStartOfDay();
        this.model = model;
    }

    public DateTime getDate() {
        return this.dateTime;
    }

    public String getModel() {
        return this.model;
    }

    public File getFile() {
        return this.file;
    }

    public Metadata getMetadata() {
        return this.metadata;
    }

    private void extractDate() {
        ExifSubIFDDirectory directory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
        if (directory != null) {
            Date date = directory.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);
            if (date != null) {
                dateTime = new DateTime(date).withTimeAtStartOfDay();
            }
        }
    }

    private void extractModel() {
        ExifIFD0Directory directory = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
        if (directory != null) {
            ExifIFD0Descriptor descriptor = new ExifIFD0Descriptor(directory);
            model = descriptor.getDescription(ExifIFD0Directory.TAG_MODEL);
            if (model == null) {
                model = "UNKNOWN";
            }
            String make = descriptor.getDescription(ExifIFD0Directory.TAG_MAKE);
            if (make == null) {
                make = "UNKNOWN";
            }
            if (!model.startsWith(make)) { // Some cameras inlcude make in model tag, so we only append make if that's needed.
                model = String.format("%s %s", make, model);
            }
        }
    }

}
