/*
 * Copyright (c) 2015, jet800
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.jet800.imagecategorizer;

import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.jet800.imagecategorizer.category.FileCategory;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

/**
 *
 * @author jet800
 */
public class StandaloneRunner {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Options options = new Options();
        options.addOption("timeout", true, "timeout in seconds to wait for sub-directory to be parsed. Default is 300.");
        options.addOption("threads", true, "size of pool to parse sub-directories, efficiently - how many sub-dirs would be parsed simulateneously."
                + " Default is equal to number of available processors.");

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();

        try {
            CommandLine cmd = parser.parse(options, args);
            String paramString = cmd.getOptionValue("timeout");
            int timeout = paramString == null ? 300 : NumberUtils.createInteger(paramString);
            paramString = cmd.getOptionValue("threads");
            int threads = paramString == null ? Runtime.getRuntime().availableProcessors() : NumberUtils.createInteger(paramString);
            List<String> argList = cmd.getArgList();
            if (argList.size() == 1) {
                File rootDir = new File(argList.get(0));
                doProcess(timeout, threads, rootDir);
            } else {
                System.err.println("Failed to parse command line: invalid parameter count");
                formatter.printHelp("StandaloneRunner <rootDirectory>", options, true);
            }
        } catch (ParseException exp) {
            System.err.println("Failed to parse command line: " + exp.getMessage());
            formatter.printHelp("StandaloneRunner <rootDirectory>", options, true);
        }

    }

    private static void doProcess(int timeout, int threads, File directory) {
        try {
            ImageDirectoryProcessor processor = new ImageDirectoryProcessor(timeout, threads, directory);

            List<List<FileCategory>> categories = processor.process();
            for (List<FileCategory> category : categories) {
                System.out.println(StringUtils.join(category, "\n"));
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(StandaloneRunner.class.getName()).log(Level.SEVERE, "Abnormal process termination", ex);
        }
    }
}
