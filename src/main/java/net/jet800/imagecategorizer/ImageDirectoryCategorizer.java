/*
 * Copyright (c) 2015, jet800
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.jet800.imagecategorizer;

import com.drew.imaging.ImageProcessingException;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import net.jet800.imagecategorizer.category.AcceptEverythingFileCategory;
import net.jet800.imagecategorizer.category.FileCategory;
import net.jet800.imagecategorizer.category.ImageCategory;
import net.jet800.imagecategorizer.category.ModelAndDateImageFileCategory;
import org.apache.commons.io.filefilter.RegexFileFilter;

/**
 * @author jet800
 */
public class ImageDirectoryCategorizer implements Callable<List<FileCategory>> {

    private static final FileFilter JPEG_FILTER = new RegexFileFilter(".*\\.jpe?g", Pattern.CASE_INSENSITIVE);

    private final File imageDirectory;

    /**
     * Creates a new <code>ImageDirectoryCategorizer</code> instance with specified directory to categorize.
     *
     * @param imageDirectory directory to categorize.
     */
    public ImageDirectoryCategorizer(File imageDirectory) {
        if (!imageDirectory.isDirectory()) {
            throw new IllegalArgumentException("Provided file is not a directory!");
        }
        this.imageDirectory = imageDirectory;
    }

    /**
     * Run categorization on given directory. Each category would contain all images sharing either same model or same
     * filming date. For images that could not be processed a separate category with name <b>notProcessed</b>
     * will be created. For images without EXIF metadata(more specifically without model/make and original date fields)
     * another category <tt>noExif</tt> will be created.
     *
     * @return List of file categories grouped by same model or filming date.
     */
    public List<FileCategory> categorize() {
        List<Image> images = new LinkedList<>();

        FileCategory notProcessed = new AcceptEverythingFileCategory("notProcessed");

        File[] files = imageDirectory.listFiles(JPEG_FILTER);
        for (File f : files) {
            try {
                images.add(new Image(f));
            } catch (ImageProcessingException | IOException ex) {
                notProcessed.accept(f);
                Logger.getLogger(ImageDirectoryCategorizer.class.getName()).log(Level.WARNING, null, ex);
            }
        }

        List<FileCategory> categories = categorize(images);
        if (notProcessed.containsFiles()) {
            categories.add(notProcessed);
        }

        return categories;
    }

    /**
     * Run categorization on given list of images. Each category would contain all images sharing either same model or
     * same filming date. For images without EXIF metadata(more specifically without model/make and original date
     * fields) another category <tt>noExif</tt> will be created.
     *
     * @param images List of images to categorize.
     * @return List of file categories grouped by same model or filming date.
     */
    public static List<FileCategory> categorize(Iterable<Image> images) {
        List<FileCategory> categories = new LinkedList<>();
        FileCategory noExif = new AcceptEverythingFileCategory("noExif");
        for (Image image : images) {
            if (image.getDate() == null || image.getModel() == null) { // if for some reason image does not contain all needed EXIF data, pass it to separate category and continue
                noExif.accept(image.getFile());
                break;
            }
            ImageCategory accepted = null;
            for (Iterator<FileCategory> iterator = categories.iterator(); iterator.hasNext();) {
                ImageCategory category = (ImageCategory) iterator.next();
                if (category.accept(image)) {
                    if (accepted == null) {
                        accepted = category;
                    } else {
                        // We've found 2 matching categories, we should merge them.
                        accepted.merge(category);
                        iterator.remove();
                        // There could be at most 2 categories that could accept same image, so no need to iterate further
                        break;
                    }
                }
            }

            if (accepted == null) { // no category accepted image, let's create new one for it
                categories.add(new ModelAndDateImageFileCategory("test", image));
            }
        }

        if (noExif.containsFiles()) {
            categories.add(noExif);
        }

        return categories;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FileCategory> call() throws Exception {
        return categorize();
    }

}
