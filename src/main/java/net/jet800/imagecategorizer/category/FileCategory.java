/*
 * Copyright (c) 2015, jet800
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.jet800.imagecategorizer.category;

import java.io.File;
import java.util.List;

/**
 *
 * @author jet800
 */
public interface FileCategory {

    /**
     * Returns <tt>true</tt> if specified file was accepted by this
     * FileCategory.
     *
     * @param file file to be tested by this FileCategory
     * @return <tt>true</tt> if specified file was accepted by this FileCategory
     */
    boolean accept(File file);

    /**
     * Returns <code>List</code> of files accepted by this FileCategory.
     *
     * @return List of files accepted by this FileCategory.
     */
    List<File> getFiles();

    /**
     * Merges content of second category to this one. No checks are guranteed be
     * made during merge.
     *
     * @param category category to be merged into this one.
     */
    void merge(FileCategory category);

    /**
     * Returns <tt>true</tt> if this FileCategory contains at least one file.
     *
     * @return <tt>true</tt> if this FileCategory contains at least one file.
     */
    boolean containsFiles();

    /**
     * Returns the number of elements in this file category. If it contains more
     * than <tt>Integer.MAX_VALUE</tt> elements, returns
     * <tt>Integer.MAX_VALUE</tt>.
     *
     * @return the number of elements in this in this file category
     */
    int size();

}
