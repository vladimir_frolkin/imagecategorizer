/*
 * Copyright (c) 2015, jet800
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.jet800.imagecategorizer.category;

import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jet800
 */
public abstract class AbstractFileCategory implements FileCategory {

    protected Set<File> files;
    protected String name;

    public AbstractFileCategory(String name) {
        this.files = new HashSet<>();
        this.name = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract boolean accept(File file);

    /**
     * {@inheritDoc}
     */
    @Override
    public List<File> getFiles() {
        return new LinkedList(files);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void merge(FileCategory category) {
        this.files.addAll(category.getFiles());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return this.files.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsFiles() {
        return !this.files.isEmpty();
    }

    /**
     * Returns a string representation of this category. Returned string is
     * composed of name of the category + colon + list of accepted files joined
     * by comma.
     *
     * @return tring representation of this category.
     */
    @Override
    public String toString() {
        return String.format("[%s]:{%s}", name, StringUtils.join(files, ","));
    }

}
