/*
 * Copyright (c) 2015, jet800
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.jet800.imagecategorizer.category;

import com.drew.imaging.ImageProcessingException;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.jet800.imagecategorizer.Image;
import net.jet800.imagecategorizer.NoNullHashSet;
import org.joda.time.DateTime;

/**
 *
 * @author jet800
 */
public class ModelAndDateImageFileCategory extends AbstractFileCategory implements ImageCategory {

    protected Set<String> models;
    protected Set<DateTime> dates;

    public ModelAndDateImageFileCategory(String name, Image image) {
        super(name);

        models = new NoNullHashSet<>();
        dates = new NoNullHashSet<>();
        models.add(image.getModel());
        dates.add(image.getDate());
        files.add(image.getFile());
    }

    /**
     * Returns <tt>true</tt> if specified file was accepted by this ImageCategory. This image category accepts files if
     * it already contains at least one file with either same model or filming date. This method would create an Image
     * instance from given file and then pass it to {@link #accept(Image)} method.
     *
     * @param file file to be tested by this FileCategory
     * @return <tt>true</tt> if specified file was accepted by this FileCategory
     * @throws IllegalArgumentException if there was an error creating image from specified file.
     * @see Image
     * @see ModelAndDateImageFileCategory#accept(Image)
     */
    @Override
    public boolean accept(File file) throws IllegalArgumentException {
        try {
            return accept(new Image(file));
        } catch (ImageProcessingException | IOException ex) {
            Logger.getLogger(ModelAndDateImageFileCategory.class.getName()).log(Level.SEVERE, null, ex);
            throw new IllegalArgumentException(ex);
        }
    }

    /**
     * Returns <tt>true</tt> if specified image was accepted by this ImageCategory. This image category accepts files if
     * it already contains at least one file with either same model or filming date.
     *
     * @param image image to be tested by this FileCategory
     * @return <tt>true</tt> if specified file was accepted by this FileCategory
     * @throws IllegalArgumentException if there was an error creating image from specified file.
     * @see Image
     *
     */
    @Override
    public boolean accept(Image image) {
        if (models.contains(image.getModel())) {
            dates.add(image.getDate());
            files.add(image.getFile());
            return true;
        } else if (dates.contains(image.getDate())) {
            models.add(image.getModel());
            files.add(image.getFile());
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void merge(FileCategory category) {
        super.merge(category);
        if (category instanceof ModelAndDateImageFileCategory) {
            ModelAndDateImageFileCategory imageFileCategory = (ModelAndDateImageFileCategory) category;
            dates.addAll(imageFileCategory.dates);
            models.addAll(imageFileCategory.models);
        }
    }

}
